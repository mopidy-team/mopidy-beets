Mopidy>=3.0.0
Pykka>=2.0.1
requests>=2.0.0
setuptools

[dev]
black
check-manifest
flake8
flake8-bugbear
flake8-import-order
isort[pyproject]
build
twine
pytest
pytest-cov

[lint]
black
check-manifest
flake8
flake8-bugbear
flake8-import-order
isort[pyproject]

[release]
build
twine

[test]
pytest
pytest-cov
